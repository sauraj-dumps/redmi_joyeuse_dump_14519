## qssi-user 11 RKQ1.200826.002 V12.5.3.0.RJZMIXM release-keys
- Manufacturer: xiaomi
- Platform: atoll
- Codename: joyeuse
- Brand: Redmi
- Flavor: qssi-user
- Release Version: 11
- Id: RKQ1.200826.002
- Incremental: V12.5.3.0.RJZMIXM
- Tags: release-keys
- CPU Abilist: arm64-v8a,armeabi-v7a,armeabi
- A/B Device: false
- Locale: en-GB
- Screen Density: 440
- Fingerprint: Redmi/joyeuse_global/joyeuse:11/RKQ1.200826.002/V12.5.3.0.RJZMIXM:user/release-keys
- OTA version: 
- Branch: qssi-user-11-RKQ1.200826.002-V12.5.3.0.RJZMIXM-release-keys
- Repo: redmi_joyeuse_dump_14519


>Dumped by [Phoenix Firmware Dumper](https://github.com/DroidDumps/phoenix_firmware_dumper)
